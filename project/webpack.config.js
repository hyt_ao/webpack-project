const path = require("path")

module.exports = {
    entry: {
        app:"./src/index.js"
    },
    output: {
        publicPath:__dirname + "/dist",
        path:path.resolve(__dirname, "dist"),
        filename:"bundle.js"
    },
    module: {//用于配置所有第三方模块加载器
        rules:[//第三方模块加载器的规则
            //处理.css文件的第三方loader规则
            {test:/\.css$/, use: ['style-loader', 'css-loader']},
            //处理.less文件的第三方loader规则
            {test:/\.less$/, use: ['style-loader', 'css-loader', 'less-loader']},
            //处理.scss文件的第三方loader规则
            {test:/\.scss$/, use: ['style-loader', 'css-loader', 'sass-loader']},
            //处理图片路径的loader
            {test:/\.(jpg|jpeg|png|pneg|gif|bmp)$/, use: 'url-loader'},
            //处理字体文件
            {test:/\.(ttf|eot|svg|woff|woff2|otf)$/, use: 'url-loader'}
        ]
    }
}